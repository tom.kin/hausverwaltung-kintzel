<?php
	get_header();
?>

	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
		<b><?php the_title(); ?></b><br />
        <button type="button" data-toggle="tooltip" data-placement="top" title="Tooltip ist cool" class="btn btn-primary">Hello</button>
        <i class="fas fa-check"></i> 
		<?php the_content(); ?>
		<hr />
	<?php endwhile; endif; ?>	
	
<?php
	get_footer();
?>