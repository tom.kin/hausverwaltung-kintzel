// Core Lib laden
import { library, dom } from "@fortawesome/fontawesome-svg-core";

// Icons importieren
import { faCheck } from "@fortawesome/free-solid-svg-icons/faCheck";

// Icon hinzufügen
library.add(faCheck);

// Aktivieren
dom.watch();