<?php

get_header();
?>
<div class="container mt-5">
	<h1 class="text-center">
		<?php the_content(); ?>	
	</h1>
</div>

<?php
get_footer();


