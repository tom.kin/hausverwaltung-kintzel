const path = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');

module.exports = {
	mode: 'production',
	entry: {
		// Basis JS und CSS
		global: './src/js/global.js',
        // Bootstrap
		bootstrap: './src/js/bootstrap.js',
		// Fontawesome
		fontawesome: './src/js/fontawesome.js',
	},
	output: {
		filename: '[name].min.js',
		path: path.resolve(__dirname, 'assets/build')
	},
	plugins: [
		new MiniCssExtractPlugin({
			filename: '../build/[name].min.css'
		}),
		new CleanWebpackPlugin(),
	],
	module: {
		rules: [
			{
				test: /\.(png|jpeg|jpg|ttf|svg|eot|woff|woff2)$/,
				use: [
					{
						loader: 'file-loader',
					}
				],
			},			
			{
				test: /\.(sa|sc|c)ss$/,
				use: [
					{
						loader: MiniCssExtractPlugin.loader,
					},
					'css-loader',
					'sass-loader',
				],
			},
		]
	  }
};