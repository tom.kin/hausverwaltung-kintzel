<?php
require __DIR__ . '/vendor/autoload.php';

/********************************************************
 * THEME SUPPORT
 ********************************************************/
function hs_add_theme_support() {
	add_theme_support( 'title-tag' );
}

add_action( 'after_setup_theme', 'hs_add_theme_support' );

function hs_menus() {
	$location = [
		'primary' => 'Desktop Menu',
		'footer'  => 'Footer Menu',
	];

	register_nav_menus( $location );
}

add_action( 'init', 'hs_menus' );

/********************************************************
 * SCRIPTS AND STYLES
 ********************************************************/
function add_theme_scripts() {
	// Global JS.
	wp_register_script( 'global-js', get_template_directory_uri() . '/assets/build/global.min.js', [ 'jquery' ], true, true );
	wp_enqueue_script( 'global-js' );

	// Bootstrap.
	wp_register_script( 'bootstrap-js', get_template_directory_uri() . '/assets/build/bootstrap.min.js', [ 'jquery' ], true, true );
	wp_enqueue_script( 'bootstrap-js' );

	// Font awesome.
	wp_register_script( 'fontawesome-js', get_template_directory_uri() . '/assets/build/fontawesome.min.js', [ 'jquery' ], true, true );
	wp_enqueue_script( 'fontawesome-js' );

	// Global CSS.
	wp_register_style( 'main-css', get_template_directory_uri() . '/assets/build/global.min.css', [], filemtime( get_template_directory() . '/assets/build/global.min.css' ), false );
	wp_enqueue_style( 'main-css' );

}
add_action( 'wp_enqueue_scripts', 'add_theme_scripts' );
