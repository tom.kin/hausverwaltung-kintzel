<!DOCTYPE html>
<html lang="de">
<head>
	<meta charset="UTF-8">
	<meta name="robots" content="noindex">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="Informationen zur Hausverwaltung Kintzel">
	<?php wp_head(); ?>
</head>
<body>

<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <div class="container-fluid">
	<a class="navbar-brand" href="<?php echo get_home_url(); ?>"><?php echo get_bloginfo( 'name' ); ?></a>
	<button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
	  <span class="navbar-toggler-icon"></span>
	</button>
	<div class="collapse navbar-collapse" id="navbarSupportedContent">
		<?php
		wp_nav_menu(
			[
				'menu'           => 'primary',
				'theme_location' => 'primary',
				'items_wrap'     => '<ul class="navbar-nav me-auto mb-2 mb-lg-0">%3$s</ul>',
				'container'      => '',
				'walker'         => new Classes\Class_Nav_Walker(),

			],
		);
		?>
	  <form class="d-flex">
		<input class="form-control me-2" type="search" placeholder="Search" aria-label="Search">
		<button class="btn btn-outline-success" type="submit">Search</button>
	  </form>
	</div>
  </div>
</nav>
